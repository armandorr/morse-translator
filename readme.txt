Compile and execute:
	g++ -o morse morse.cc
	./morse

Usage:
	Select [1] for morse-spanish or [2] for spanish-morse translation.
	You can translate:
		- Alphabet letters
		- Numbers
		- Puntuaction marks: ".,;:?"

	[1] Set spaces as '/' and endlines as '//'. 
	    End your text with .-.-.
	
	[2] End your text with *
	 