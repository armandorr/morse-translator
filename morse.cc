#include <iostream>
#include <vector>
#include <queue>
#include <map>

using namespace std;

vector<string> ABC{".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};
vector<string> NiS{"-----",".----","..---","...--","....-",".....","-....","--...","---..","----.",".-.-.-","--..--","-.-.-.","---...","..--.."};
map<string,int> ABCMAP{{".-",0},{"-...",1},{"-.-.",2},{"-..",3},{".",4},{"..-.",5},{"--.",6},{"....",7},{"..",8},{".---",9},{"-.-",10},{".-..",11},{"--",12},{"-.",13},{"---",14},{".--.",15},{"--.-",16},{".-.",17},{"...",18},{"-",19},{"..-",20},{"...-",21},{".--",22},{"-..-",23},{"-.--",24},{"--..",25}};
map<string,int> NMAP{{"-----",0},{".----",1},{"..---",2},{"...--",3},{"....-",4},{".....",5},{"-....",6},{"--...",7},{"---..",8},{"----.",9}};
    
void error_y_exit(string s){
    cout << s << endl;
    exit(1);
}

void write_queue_string(queue<string>& Q){
    while(not Q.empty()){
        cout << Q.front() << " ";
        Q.pop();
    }
    cout << ".-.-." << endl; //final de mensaje
}

void write_queue_char(queue<char>& Q){
    while(not Q.empty()){
        cout << Q.front();
        Q.pop();
    }
    cout << "*" << endl; //final de mensaje
}

int main(int argc, char* argv[]){
    if(argc != 2) error_y_exit("Usage: program.exe [1,2] (1= Morse->Spanish, 2= Spanish->Morse)");
    int mode = int(argv[1][0]-'0');
    string s;
    if(mode == 1){ //Morse->Español
        queue<char> Q;
        while(cin >> s and s!=".-.-."){ //final mensaje
            if (s == "//") Q.push(10); //salto de linea
            else if(s == "/") Q.push(32); //espacio
            else{
                map<string,int>::iterator it;
                if(s == ".-.-.-") Q.push('.');
                else if(s == "--..--") Q.push(',');
                else if(s == "-.-.-.") Q.push(';');
                else if(s == "---...") Q.push(':');
                else if(s == "..--..") Q.push('?');
                else{
                    it = ABCMAP.find(s);
                    if(it != ABCMAP.end()) Q.push(((*it).second)+'a');
                    else{
                        it = NMAP.find(s);
                        if(it != NMAP.end()) Q.push(((*it).second)+'0');
                        else error_y_exit("No traducible");
                    }
                }
            }
        }
        write_queue_char(Q);
    }
    else{ //Español->Morse
        queue<string> Q;
        bool finished = false;
        while(not finished and getline(cin,s)){
            for(int i = 0; i < s.size() and not finished; ++i){
                char c = s[i];
                int value;
                if(c == '*') finished = true;
                else if(c == 32) Q.push("/");
                else if (c >= 97 and c <= 122){
                    value = c-'a';
                    Q.push(ABC[value]);
                }
                else{
                    if(c >= 48 and c <= 57) value = c-'0'; //numero
                    else if(c == 46) value = 10; //.
                    else if (c == 44) value = 11; //,
                    else if (c == 59) value = 12; //;
                    else if (c == 58) value = 13; //:
                    else if (c == 63) value = 14; //?
                    else error_y_exit("No traducible");
                    Q.push(NiS[value]);   
                }
            }
            if (not finished) Q.push("//");
        }
        write_queue_string(Q);
    }
}
